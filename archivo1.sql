CREATE DATABASE db_contactos

use db_contactos

GO

-- dbo.Contacto definition

-- Drop table

-- DROP TABLE dbo.Contacto;

CREATE TABLE dbo.Contacto (
	id bigint NOT NULL,
	nombre nvarchar(100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	email nvarchar(50) COLLATE Modern_Spanish_CI_AS NOT NULL,
	fechanac date NULL,
	mensaje nvarchar(MAX) COLLATE Modern_Spanish_CI_AS NOT NULL,
	CONSTRAINT PK__Contacto__3213E83FAB38E427 PRIMARY KEY (id)
);

CREATE TABLE dbo.Contacto2 (
	id bigint NOT NULL,
	nombre nvarchar(100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	email nvarchar(50) COLLATE Modern_Spanish_CI_AS NOT NULL,
	fechanac date NULL,
	mensaje nvarchar(MAX) COLLATE Modern_Spanish_CI_AS NOT NULL,
	CONSTRAINT PK__Contacto__3213E83FAB38E427 PRIMARY KEY (id)
);

CREATE TABLE dbo.Contacto3 (
	id bigint NOT NULL,
	nombre nvarchar(100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	email nvarchar(50) COLLATE Modern_Spanish_CI_AS NOT NULL,
	fechanac date NULL,
	mensaje nvarchar(MAX) COLLATE Modern_Spanish_CI_AS NOT NULL,
	CONSTRAINT PK__Contacto__3213E83FAB38E427 PRIMARY KEY (id)
);

-- dbo.Usuario definition

-- Drop table

-- DROP TABLE dbo.Usuario;

CREATE TABLE db_servicios_generales.dbo.Usuario (
	username nvarchar(20) COLLATE Modern_Spanish_CI_AS NOT NULL,
	password nvarchar(100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	CONSTRAINT PK__Usuario__F3DBC5734C2D8C7D PRIMARY KEY (username)
);


CREATE TABLE db_servicios_generales.dbo.Usuario2 (
	username nvarchar(20) COLLATE Modern_Spanish_CI_AS NOT NULL,
	password nvarchar(100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	CONSTRAINT PK__Usuario__F3DBC5734C2D8C7D PRIMARY KEY (username)
);

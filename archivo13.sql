
CREATE TABLE dbo.Contacto (
	id bigint NOT NULL,
	nombre nvarchar(100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	email nvarchar(50) COLLATE Modern_Spanish_CI_AS NOT NULL,
	fechanac date NULL,
	mensaje nvarchar(MAX) COLLATE Modern_Spanish_CI_AS NOT NULL,
	CONSTRAINT PK__Contacto__3213E83FAB38E427 PRIMARY KEY (id)
);

CREATE TABLE dbo.Contacto2 (
	id bigint NOT NULL,
	nombre nvarchar(100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	email nvarchar(50) COLLATE Modern_Spanish_CI_AS NOT NULL,
	fechanac date NULL,
	mensaje nvarchar(MAX) COLLATE Modern_Spanish_CI_AS NOT NULL,
	CONSTRAINT PK__Contacto__3213E83FAB38E427 PRIMARY KEY (id)
);
